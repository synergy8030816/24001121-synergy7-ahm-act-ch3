package synergy.challenge.searchwords

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import synergy.challenge.searchwords.databinding.FragmentHomePageBinding
import synergy.challenge.searchwords.databinding.FragmentPageListBinding

class PageList : Fragment() {
    private var _binding: FragmentPageListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPageListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val params = arguments?.getString(HomePage.PARAMS)
        var dataList = params?.let { mutableListOf(params) }
        when(params){
            Words.A.char -> {dataList = Words.A.words}
            Words.B.char -> {dataList = Words.B.words}
            Words.C.char -> {dataList = Words.C.words}
            Words.D.char -> {dataList = Words.D.words}
            Words.E.char -> {dataList = Words.E.words}
            Words.F.char -> {dataList = Words.F.words}
            Words.G.char -> {dataList = Words.G.words}
            Words.H.char -> {dataList = Words.H.words}
            Words.I.char -> {dataList = Words.I.words}
            Words.J.char -> {dataList = Words.J.words}
        }
        val adapter = AlphabetAdapter{
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse("https://www.google.com/search?q=$it"))
            activity?.startActivity(intent)
        }
        dataList?.let { adapter.submitData(dataList) }
        binding.rvListHomePage.adapter = adapter
        binding.rvListHomePage.layoutManager = LinearLayoutManager(activity)
        binding.arrowBackPageList.setOnClickListener {
            findNavController().navigate(R.id.homePage)
        }

    }


}