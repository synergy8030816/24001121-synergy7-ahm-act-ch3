package synergy.challenge.searchwords

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import synergy.challenge.searchwords.databinding.FragmentHomePageBinding

class HomePage : Fragment() {
    private var _binding: FragmentHomePageBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomePageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()

    }

    private fun setupRecyclerView() {
        val adapter = AlphabetAdapter{
            val bundle = Bundle()
            bundle.putString(PARAMS, it)
            findNavController().navigate(R.id.pageList, bundle)
        }

        adapter.submitData(alphabetWords)
        binding.rvListHomePage.adapter = adapter
        binding.rvListHomePage.layoutManager = LinearLayoutManager(activity)

        binding.btnToGridHomePage.setOnClickListener {
            binding.rvListHomePage.layoutManager = GridLayoutManager(activity, 3)
        }
    }
    companion object {
        const val PARAMS = "PARAMS"
    }


}