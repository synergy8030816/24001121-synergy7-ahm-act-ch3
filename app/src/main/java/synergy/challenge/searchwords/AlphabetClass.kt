package synergy.challenge.searchwords

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class AlphabetClass(val alpha: String) : Parcelable

val alphabetWords = ('A'..'J').map { it.toString() }.toMutableList()
enum class Words(val char: String, val words: ArrayList<String>){
    A("A", arrayListOf("Anjing", "Anggaran", "Awan", "Angan-angan", "Anjay")),
    B("B", arrayListOf("Bandung", "Biawak", "Binjai", "Berdoa", "Belanda")),
    C("C", arrayListOf("Cantik", "Cerdas", "Cakwe", "Cangkeman", "Cleopatra")),
    D("D", arrayListOf("Dendeng", "Daging", "Dagu", "Depok", "Dana")),
    E("E", arrayListOf("Elang", "Eskimo", "Elok", "Emas", "Estimasi")),
    F("F", arrayListOf("Flamingo", "France", "Fantastic", "Flamboyan", "Fundamental")),
    G("G", arrayListOf("Gajah", "Gunung", "Gading", "Gelato", "Gedung")),
    H("H", arrayListOf("Haji", "Harimau", "Hambar", "Hak", "Hiburan")),
    I("I", arrayListOf("Ikan", "Intan", "Indah", "Imut", "Iblis")),
    J("J", arrayListOf("Jakarta", "Jahat", "Jahannam", "Jika", "Jauh"))
}
